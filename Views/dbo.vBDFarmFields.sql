SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vBDFarmFields] as 
Select *
From (SELECT 
case when [Email_BusinessTypeValidationSupported] = 'Yes'
and [Email_Business2TypeValidationSupported] != 'Yes'
	then [Email_BusinessType]
     when [Email_Business2TypeValidationSupported] = 'Yes'
	 and [Email_BusinessTypeValidationSupported] != 'Yes'
	 then [Email_Business2Type]
	 when [Email_BusinessTypeValidationSupported] = 'Yes'
		and [Email_Business2TypeValidationSupported] = 'Yes'
		then [Email_BusinessType]
		when [Email_BusinessTypeValidationSupported] = 'No'
		and [Email_Business2TypeValidationSupported] != 'Yes'
		then [Email_BusinessType]
		when [Email_BusinessTypeValidationSupported] = ''
		and [Email_Business2TypeValidationSupported] != ''
		then [Email_Business2Type]
		end as FarmEmail
      ,[FirstName]
      ,[MiddleName]
      ,[LastName]
      ,[Suffix]
      ,[RepCRD]
  FROM [DiscoveryData].[dbo].[BDFarmFields]) FarmFields
  where FarmFields.FarmEmail is not null
GO
