SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[vSFUpload_EmailPopulation] as
with cte_Discovery_Email_Prep as (
SELECT case when Branch_PhoneDoNotCall != 'DNC'
		THEN [Branch_Phone]
		else Null
		end as [Discovery Branch Phone]
      ,case when [Email_BusinessType] != ''
	  then [Email_BusinessType]
	  end as [Discovery Email]
	  ,case 
	  when [Email_Business2Type] != ''
	  then [Email_Business2Type]
	  end as [Discovery Secondary Email]
	  ,case when [Email_PersonalType] != ''
	  then [Email_PersonalType]
	  else null
	  end as [Discovery Personal Email]
	  ,[PersonalWebpage]
      ,[FirmWebsite]
      ,[SocialMedia_LinkedIn]
	  ,sf.email
	  ,sf.phone
	  
      ,[RepCRD]
	  ,sf.id
  FROM [DiscoveryData].[dbo].[Independent Financial Partners] c
  join openquery(SALESFORCEDEVART, 'select id, CRD__c, Email, Phone, Broker_Dealer__c from Contact where CRD__c != null') sf on sf.CRD__c = c.RepCRD
  join openquery(SALESFORCEDEVART, 'select id, Firm_CRD__c from Broker_Dealer__c where Firm_CRD__c != null') sf_firm on sf.Broker_Dealer__c = sf_Firm.id
  --where sf_firm.Firm_CRD__c  in ('160209')
--'26898')
  )
  
  select distinct c.id,
  ISNULL([Discovery Branch Phone],'') as [Discovery Branch Phone],
  ISNULL([Discovery Email],'') as [Discovery Email],
  ISNULL([Discovery Secondary Email],'') as [Discovery Secondary Email],
  ISNULL([Discovery Personal Email],'') as [Discovery Personal Email],
  ISNULL([PersonalWebpage],'') as [Discovery Personal Web Page]
  ,ISNULL([FirmWebsite],'') as [Discovery Firm Website]
  ,ISNULL([SocialMedia_LinkedIn] ,'') as [Discovery LinkedIn Page]
  ,
  case when ISNULL(c.email,'') = ''
		and ISNULL([Discovery Email],'') != ''
	  then [Discovery Email] 
	  when ISNULL(c.email,'')  = ''
		and ISNULL([Discovery Email],'') = ''
		and [Discovery Secondary Email] != ''
		then [Discovery Secondary Email]
	  else ISNULL(c.email , '')
	  end as email
  ,[RepCRD]
  From cte_Discovery_Email_Prep c
  join openquery(SALESFORCEDEVART, 'select id, CRD__c, Email, Phone, MobilePhone, OtherPhone, Other_Email_Addresses__c, Broker_Dealer__c from Contact where CRD__c != null') sf on sf.CRD__c = c.RepCRD
  join openquery(SALESFORCEDEVART, 'select id, Firm_CRD__c from Broker_Dealer__c where Firm_CRD__c != null') bd on bd.id = sf.Broker_Dealer__c
GO
