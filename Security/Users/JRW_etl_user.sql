IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'JRW\etl_user')
CREATE LOGIN [JRW\etl_user] FROM WINDOWS
GO
CREATE USER [JRW\etl_user] FOR LOGIN [JRW\etl_user]
GO
