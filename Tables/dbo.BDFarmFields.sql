CREATE TABLE [dbo].[BDFarmFields]
(
[Email_BusinessType] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_BusinessTypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_BusinessTypeUpdate] [datetime] NULL,
[Email_Business2Type] [varchar] (48) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Business2TypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Business2TypeUpdate] [datetime] NULL,
[FirstName] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RepCRD] [bigint] NULL
) ON [PRIMARY]
GO
