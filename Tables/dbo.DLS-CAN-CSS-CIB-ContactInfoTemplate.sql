CREATE TABLE [dbo].[DLS-CAN-CSS-CIB-ContactInfoTemplate]
(
[Branch_Phone] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_PhoneType] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_PhoneDoNotCall] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Business2Type] [varchar] (38) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Business2TypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_PersonalType] [varchar] (34) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_PersonalTypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_BusinessType] [varchar] (39) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_BusinessTypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PersonalWebpage] [varchar] (83) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmWebsite] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SocialMedia_LinkedIn] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RepCRD] [bigint] NULL
) ON [PRIMARY]
GO
