CREATE TABLE [dbo].[ContactInfoTemplate-M Holdings Combined]
(
[Branch_Phone] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_PhoneType] [varchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_PhoneDoNotCall] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Business2Type] [varchar] (49) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Business2TypeValidationSupported] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_PersonalType] [varchar] (37) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_PersonalTypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_BusinessType] [varchar] (38) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_BusinessTypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PersonalWebpage] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmWebsite] [varchar] (34) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SocialMedia_LinkedIn] [varchar] (103) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RepCRD] [bigint] NULL
) ON [PRIMARY]
GO
