CREATE TABLE [dbo].[ContactInfoTemplate-Kalos-Benchmark-Arkadios]
(
[Branch_Phone] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_PhoneType] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_PhoneDoNotCall] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Business2Type] [varchar] (47) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Business2TypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_PersonalType] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_PersonalTypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_BusinessType] [varchar] (34) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_BusinessTypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PersonalWebpage] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmWebsite] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SocialMedia_LinkedIn] [varchar] (77) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RepCRD] [int] NULL
) ON [PRIMARY]
GO
