CREATE TABLE [dbo].[TitanSecuritiesContactInfo]
(
[Branch_Phone] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_PhoneType] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_PhoneDoNotCall] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Business2Type] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Business2TypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_PersonalType] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_PersonalTypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_BusinessType] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_BusinessTypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PersonalWebpage] [varchar] (66) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmWebsite] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SocialMedia_LinkedIn] [varchar] (54) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RepCRD] [int] NULL
) ON [PRIMARY]
GO
