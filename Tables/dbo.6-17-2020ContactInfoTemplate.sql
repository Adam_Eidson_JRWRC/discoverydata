CREATE TABLE [dbo].[6-17-2020ContactInfoTemplate]
(
[Branch_Phone] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_PhoneType] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Branch_PhoneDoNotCall] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Business2Type] [varchar] (48) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Business2TypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_PersonalType] [varchar] (38) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_PersonalTypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_BusinessType] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_BusinessTypeValidationSupported] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PersonalWebpage] [varchar] (145) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmWebsite] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SocialMedia_LinkedIn] [varchar] (99) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RepCRD] [bigint] NULL
) ON [PRIMARY]
GO
